/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

using System;

namespace Passgen
{
    class MainClass
    {
	public static void Main(string[] args)
	{
	    if(args.Length == 0) {
		Console.WriteLine("ARGUMENT ERROR: Please specificy desired password length.");
		return;
	    }
	    int length = int.Parse(args[0]);
	    int[] salt;
	    byte[] buffer;
	    string password;
	    Random rand = new Random(DateTime.Now.GetHashCode());
	    //Console.Write("Enter desired character length: ");
	    //length = int.Parse(Console.ReadLine());
	    salt = new int[length];
	    buffer = new byte[length];
	    for(int i = 0; i < length; ++i) {
		salt[i] = rand.Next(32, 128);
	    }
	    for(int i = 0; i < length; i++) {
		buffer[i] = Convert.ToByte(salt[i]);
	    }
	    password = System.Text.Encoding.ASCII.GetString(buffer);
	    Console.WriteLine(String.Format("{0}Password: {1}", Environment.NewLine, password));
	}
    }
}
